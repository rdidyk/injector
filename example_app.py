import contextlib
import sqlite3 as lite
from datetime import datetime
from injector import InjectorContainer
from pprint import pprint

config = {
    'db_uri': ':memory:',
}

registry = InjectorContainer()

registry.add_service(lambda _: config, name='settings')


class BooleanType(object):

    @staticmethod
    def adapt(value):
        return 'True' if value else 'False'

    @staticmethod
    def convert(value):
        return True if value == 'True' else False


class TodoModel(object):

    __slots__ = ['id', 'title', 'done', 'created', 'updated']

    def __init__(self, **attributes):
        self.id = attributes.get('id')
        self.title = attributes.get('title')
        self.done = attributes.get('done', False)
        self.created = attributes.get('created', datetime.utcnow())
        self.updated = attributes.get('updated', datetime.utcnow())

    def as_dict(self):
        return {
            'id': self.id,
            'title': self.title,
            'done': self.done,
            'created': self.created,
            'updated': self.updated,
        }

    @staticmethod
    def factory(_, data):
        fields = TodoModel.__slots__
        return TodoModel(**dict(zip(fields, data)))

    def __repr__(self):
        return "TodoModel(id={id}, title='{title}', done={done}, " \
               "created={created}, updated={updated})".format(
                    **self.as_dict()
                )


class ICRUDStorage(object):

    def create(self, *args, **kwargs):
        raise NotImplemented()

    def read(self, *args, **kwargs):
        raise NotImplemented()

    def update(self, *args, **kwargs):
        raise NotImplemented()

    def delete(self, *args, **kwargs):
        raise NotImplemented()


class GenericSQLiteStorage(ICRUDStorage):

    def __init__(self, db_conn, row_factory=None):
        """
        :type db_conn: sqlite3.Connection
        """
        self._db = db_conn
        self._default_row_factory = row_factory or lite.Row

    def get_cursor(self):
        return self._db.cursor()

    def create(self, table, data):
        with contextlib.closing(self.get_cursor()) as db:
            query = 'INSERT INTO {} ({}) VALUES ({});'.format(
                table,
                ','.join(data.keys()),
                ','.join(map(lambda x: ':'+x, data.keys()))
            )
            try:
                inserted_id = db.execute(
                    query,
                    data
                ).lastrowid
            except:
                db.connection.rollback()
                raise
            else:
                db.connection.commit()
        return inserted_id

    def update(self, table, data, condition=None):
        with contextlib.closing(self.get_cursor()) as db:
            values = data.values()
            query = 'UPDATE {0} SET {1}'.format(
                table,
                ','.join(['%s=?' % k for k in data.keys()]),
            )
            if condition:
                query += ' WHERE {};'.format(
                    ' AND '.join(['%s=?' % k for k in condition.keys()]),
                )
                values += condition.values()
            else:
                query += ';'
            try:
                result = db.execute(
                    query,
                    values
                )
            except:
                db.connection.rollback()
                raise
            else:
                db.connection.commit()
        return result

    def read(self, table, condition=None, fields=None, row_factory=None):
        with contextlib.closing(self.get_cursor()) as db:
            db.row_factory = row_factory or self._default_row_factory
            fields = fields or ['*']
            query = 'SELECT {0} FROM {1}'.format(
                ', '.join(fields),
                table
            )
            values = []
            if condition:
                query += ' WHERE {};'.format(
                    ' AND '.join(['%s=?' % k for k in condition.keys()]),
                )
                values += condition.values()
            else:
                query += ';'
            result = db.execute(query, values).fetchall()
        return result

    def delete(self, table, condition=None):
        with contextlib.closing(self.get_cursor()) as db:
            values = []
            query = 'DELETE FROM {0}'.format(
                table,
            )
            if condition:
                query += ' WHERE {};'.format(
                    ' AND '.join(['%s=?' % k for k in condition.keys()]),
                )
                values += condition.values()
            else:
                query += ';'
            try:
                result = db.execute(
                    query,
                    values
                )
            except:
                db.connection.rollback()
                raise
            else:
                db.connection.commit()
        return result


@registry.service(name='connection')
def db_connection(_, settings):
    lite.register_adapter(bool, BooleanType.adapt)
    lite.register_converter("BOOLEAN", BooleanType.convert)

    lite.register_adapter(
        datetime, lambda d: d.replace(microsecond=0).isoformat(sep=' ')
    )
    lite.register_converter(
        "DATETIME", lambda d: datetime.strptime(d, '%Y-%m-%d %H:%M:%S')
    )

    conn = lite.connect(
        settings.get('db_uri', ':memory:'),
        detect_types=lite.PARSE_DECLTYPES | lite.PARSE_COLNAMES
    )
    with contextlib.closing(conn.cursor()) as cursor:
        cursor.execute("""
            CREATE TABLE IF NOT EXISTS todo(
              id INTEGER PRIMARY KEY,
              title TEXT,
              done BOOLEAN,
              created DATETIME,
              updated DATETIME DEFAULT CURRENT_TIMESTAMP
            );
        """)
    return conn


@registry.service(name='storage')
def db_storage(_, connection):
    return GenericSQLiteStorage(connection)


class TodoApplication(object):

    @registry.inject
    def add(self, storage, **attributes):
        """
        :type storage: GenericSQLiteStorage
        :rtype: TodoModel
        """
        _id = storage.create('todo', TodoModel(**attributes).as_dict())
        return storage.read(
            'todo', dict(id=_id), row_factory=TodoModel.factory
        )[0]

    @registry.inject
    def update(self, model, storage):
        """
        :type storage: GenericSQLiteStorage
        :rtype: TodoModel
        """
        tid = model.id
        data = model.as_dict()
        data.pop('id')
        storage.update('todo', data, dict(id=tid))
        return model

    @registry.inject
    def remove(self, tid, storage):
        """
        :type storage: GenericSQLiteStorage
        """
        storage.delete('todo', dict(id=tid))

    @registry.inject
    def list(self, storage, filters=None):
        """
        :type storage: GenericSQLiteStorage
        :rtype: list[TodoModel]
        """
        conditions = filters or {}
        return storage.read(
            'todo',
            condition=conditions,
            row_factory=TodoModel.factory
        )

if __name__ == '__main__':

    example_app = TodoApplication()

    t1 = example_app.add(title='test 1')
    t2 = example_app.add(title='test 2')
    t3 = example_app.add(title='test 3')

    print "Created entry #1: ", t1
    print "Created entry #2: ", t2
    print "Created entry #3: ", t3

    t2.done = True
    t2.title = 'test 2 [completed]'

    print "\nUpdate entry #2: ", example_app.update(t2), "\n"

    print "Collection of entries: "
    pprint(example_app.list())
    print "\n"

    print "Removing 3rd entry...\n"
    example_app.remove(t3.id)

    print "Collection of entries: "
    pprint(example_app.list())
    print "\n"
