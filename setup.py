from setuptools import setup
from setuptools import find_packages


setup(
    name='injector',
    version='0.1.0',
    packages=find_packages(include=('*',)),
    include_package_data=True,
    classifiers=[
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Software Development :: Libraries',
    ],
    install_requires=open('requirements.txt').read(),
    zip_safe=False,
    url='https://bitbucket.org/rdidyk/injector',
    author='Ruslan Didyk',
    author_email='rdidyk@tmgtop.com',
    description='Dependencies Container with injectors'
)
